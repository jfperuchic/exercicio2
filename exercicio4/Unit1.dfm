object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 287
  ClientWidth = 512
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 280
    Top = 36
    Width = 61
    Height = 13
    Caption = 'Identificador'
  end
  object Label2: TLabel
    Left = 280
    Top = 68
    Width = 46
    Height = 13
    Caption = 'Treinador'
  end
  object Label3: TLabel
    Left = 280
    Top = 104
    Width = 23
    Height = 13
    Caption = 'N'#237'vel'
  end
  object Label4: TLabel
    Left = 280
    Top = 136
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  object DBText1: TDBText
    Left = 400
    Top = 36
    Width = 65
    Height = 17
    DataField = 'id'
    DataSource = DataModule3.DataSource2
  end
  object DBText2: TDBText
    Left = 400
    Top = 68
    Width = 65
    Height = 17
    DataField = 'id_treinador'
    DataSource = DataModule3.DataSource2
  end
  object DBText3: TDBText
    Left = 400
    Top = 104
    Width = 65
    Height = 17
    DataField = 'nivel'
    DataSource = DataModule3.DataSource2
  end
  object DBText4: TDBText
    Left = 400
    Top = 136
    Width = 65
    Height = 17
    DataField = 'nome'
    DataSource = DataModule3.DataSource2
  end
  object btn_inserir: TButton
    Left = 311
    Top = 177
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 0
    OnClick = btn_inserirClick
  end
  object btn_editar: TButton
    Left = 311
    Top = 208
    Width = 75
    Height = 25
    Caption = 'Editar'
    TabOrder = 1
    OnClick = btn_editarClick
  end
  object btn_deletar: TButton
    Left = 311
    Top = 239
    Width = 75
    Height = 25
    Caption = 'Deletar'
    TabOrder = 2
    OnClick = btn_deletarClick
  end
  object DBLookupListBox1: TDBLookupListBox
    Left = 8
    Top = 8
    Width = 225
    Height = 264
    KeyField = 'id'
    ListField = 'nome'
    ListSource = DataModule3.DataSource2
    TabOrder = 3
  end
end
